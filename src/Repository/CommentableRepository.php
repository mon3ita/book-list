<?php

namespace App\Repository;

use App\Entity\Commentable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentable[]    findAll()
 * @method Commentable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentable::class);
    }

    // /**
    //  * @return Commentable[] Returns an array of Commentable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commentable
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
