<?php

namespace App\Repository;

use App\Entity\AuthorRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AuthorRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthorRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthorRating[]    findAll()
 * @method AuthorRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuthorRating::class);
    }

    // /**
    //  * @return AuthorRating[] Returns an array of AuthorRating objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AuthorRating
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
