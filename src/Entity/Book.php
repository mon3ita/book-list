<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Commentable;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 * @UniqueEntity(fields={"name"}, message="Book with given name already exist")
 */
class Book extends Commentable
{

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="date")
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity=Genre::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     */
    private $genre;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="favourite_books")
     */
    private $favourite_to;

    /**
     * @ORM\OneToMany(targetEntity=BookRating::class, mappedBy="book", orphanRemoval=true)
     */
    private $bookRatings;

    public function __construct()
    {
        $this->favourite_to = new ArrayCollection();
        $this->bookRatings = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {

        $this->author = $author;

        return $this;
    }

    public function getPublished(): ?\DateTimeInterface
    {
        return $this->published;
    }

    public function setPublished(\DateTimeInterface $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFavouriteTo(): Collection
    {
        return $this->favourite_to;
    }

    public function addFavouriteTo(User $favouriteTo): self
    {
        if (!$this->favourite_to->contains($favouriteTo)) {
            $this->favourite_to[] = $favouriteTo;
        }

        return $this;
    }

    public function removeFavouriteTo(User $favouriteTo): self
    {
        $this->favourite_to->removeElement($favouriteTo);

        return $this;
    }

    /**
     * @return Collection|BookRating[]
     */
    public function getBookRatings(): Collection
    {
        return $this->bookRatings;
    }

    public function addBookRating(BookRating $bookRating): self
    {
        if (!$this->bookRatings->contains($bookRating)) {
            $this->bookRatings[] = $bookRating;
            $bookRating->addBookRating($this);
        }

        return $this;
    }

    public function removeBookRating(BookRating $bookRating): self
    {
        if ($this->bookRatings->removeElement($bookRating)) {
            $bookRating->removeBookRating($this);
        }

        return $this;
    }

}
