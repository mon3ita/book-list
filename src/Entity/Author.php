<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Commentable;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 * @UniqueEntity(fields={"name"}, message="Author with given name already exist")
 */
class Author extends Commentable
{

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="author", orphanRemoval=true)
     */
    private $books;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biography;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=AuthorRating::class, mappedBy="author", orphanRemoval=true)
     */
    private $authorRatings;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="favourite_authors")
     */
    private $favourite_to;

    public function __construct()
    {
        $this->books = new ArrayCollection();
        $this->authorRatings = new ArrayCollection();
        $this->favourite_to = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }

        return $this;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): self
    {
        $this->biography = $biography;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|AuthorRating[]
     */
    public function getAuthorRatings(): Collection
    {
        return $this->authorRatings;
    }

    public function addAuthorRating(AuthorRating $authorRating): self
    {
        if (!$this->authorRatings->contains($authorRating)) {
            $this->authorRatings[] = $authorRating;
            $authorRating->setAuthor($this);
        }

        return $this;
    }

    public function removeAuthorRating(AuthorRating $authorRating): self
    {
        if ($this->authorRatings->removeElement($authorRating)) {
            // set the owning side to null (unless already changed)
            if ($authorRating->getAuthor() === $this) {
                $authorRating->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFavouriteTo(): Collection
    {
        return $this->favourite_to;
    }

    public function addFavouriteTo(User $favouriteTo): self
    {
        if (!$this->favourite_to->contains($favouriteTo)) {
            $this->favourite_to[] = $favouriteTo;
            $favouriteTo->addFavouriteAuthor($this);
        }

        return $this;
    }

    public function removeFavouriteTo(User $favouriteTo): self
    {
        if ($this->favourite_to->removeElement($favouriteTo)) {
            $favouriteTo->removeFavouriteAuthor($this);
        }

        return $this;
    }

}
