<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

use App\Event\Listener\UserListener;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\EntityListeners({"App\Event\Listener\UserListener"})
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToOne(targetEntity=UserProfile::class, mappedBy="users", cascade={"persist", "remove"})
     */
    private $user_profile;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, mappedBy="favourite_to")
     */
    private $favourite_books;

    /**
     * @ORM\OneToMany(targetEntity=BookRating::class, mappedBy="users", orphanRemoval=true)
     */
    private $bookRatings;

    /**
     * @ORM\OneToMany(targetEntity=AuthorRating::class, mappedBy="users", orphanRemoval=true)
     */
    private $authorRatings;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="users")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="favourite_to")
     */
    private $favourite_authors;

    public function __construct()
    {
        $this->votes = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->favourite_books = new ArrayCollection();
        $this->bookRatings = new ArrayCollection();
        $this->authorRatings = new ArrayCollection();
        $this->favourite_authors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserProfile(): ?UserProfile
    {
        return $this->user_profile;
    }

    public function setUserProfile(UserProfile $user_profile): self
    {
        // set the owning side of the relation if necessary
        if ($user_profile->getUsers() !== $this) {
            $user_profile->setUsers($this);
        }

        $this->user_profile = $user_profile;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getFavouriteBooks(): Collection
    {
        return $this->favourite_books;
    }

    public function addFavouriteBook(Book $favouriteBook): self
    {
        if (!$this->favourite_books->contains($favouriteBook)) {
            $this->favourite_books[] = $favouriteBook;
            $favouriteBook->addFavouriteTo($this);
        }

        return $this;
    }

    public function removeFavouriteBook(Book $favouriteBook): self
    {
        if ($this->favourite_books->removeElement($favouriteBook)) {
            $favouriteBook->removeFavouriteTo($this);
        }

        return $this;
    }

    /**
     * @return Collection|BookRating[]
     */
    public function getBookRatings(): Collection
    {
        return $this->bookRatings;
    }

    public function addBookRating(BookRating $bookRating): self
    {
        if (!$this->bookRatings->contains($bookRating)) {
            $this->bookRatings[] = $bookRating;
            $bookRating->setUsers($this);
        }

        return $this;
    }

    public function removeBookRating(BookRating $bookRating): self
    {
        if ($this->bookRatings->removeElement($bookRating)) {
            // set the owning side to null (unless already changed)
            if ($bookRating->getUsers() === $this) {
                $bookRating->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AuthorRating[]
     */
    public function getAuthorRatings(): Collection
    {
        return $this->authorRatings;
    }

    public function addAuthorRating(AuthorRating $authorRating): self
    {
        if (!$this->authorRatings->contains($authorRating)) {
            $this->authorRatings[] = $authorRating;
            $authorRating->setUsers($this);
        }

        return $this;
    }

    public function removeAuthorRating(AuthorRating $authorRating): self
    {
        if ($this->authorRatings->removeElement($authorRating)) {
            // set the owning side to null (unless already changed)
            if ($authorRating->getUsers() === $this) {
                $authorRating->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUsers($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUsers() === $this) {
                $comment->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getFavouriteAuthors(): Collection
    {
        return $this->favourite_authors;
    }

    public function addFavouriteAuthor(Author $favouriteAuthor): self
    {
        if (!$this->favourite_authors->contains($favouriteAuthor)) {
            $this->favourite_authors[] = $favouriteAuthor;
        }

        return $this;
    }

    public function removeFavouriteAuthor(Author $favouriteAuthor): self
    {
        $this->favourite_authors->removeElement($favouriteAuthor);

        return $this;
    }
}
