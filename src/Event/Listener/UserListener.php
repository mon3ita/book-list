<?php

namespace App\Event\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use App\Entity\User;
use App\Entity\UserProfile;

class UserListener {

    public function postPersist(User $user, LifecycleEventArgs $event) {

        $user_profile = new UserProfile();
        $user_profile->setUsers($user);
        
        $entityManager = $event->getEntityManager();
        $entityManager->persist($user_profile);
        $entityManager->flush();
    }
}