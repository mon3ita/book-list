<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Entity\Author;
use App\Entity\Genre;

class BookFormType extends AbstractType
{   
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('cover', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('published', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('authors', ChoiceType::class, [
                'choices' => $this->getAuthors(),
                'mapped' => false,
            ])
            ->add('genres', ChoiceType::class, [
                'choices' => $this->getGenres(),
                'mapped' => false,
                'attr' => ['style' => "margin-bottom: 10px"],
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }

    private function getAuthors() {
        $results = $this->entityManager
            ->createQueryBuilder('author')
            ->select('author')
            ->from(Author::class, 'author')
            ->getQuery()
            ->getResult();


        $authors = array();
        foreach($results as $author) {
            $authors[$author->getName()] = $author->getName();
        }

        return $authors;
    }

    private function getGenres() {
        $results = $this->entityManager
            ->createQueryBuilder('genre')
            ->select('genre')
            ->from(Genre::class, 'genre')
            ->getQuery()
            ->getResult();


        $genres = array();
        foreach($results as $genre) {
            $genres[$genre->getName()] = $genre->getName();
        }

        if(!count($genres)) {
            
        }

        return $genres;
    }    
}
