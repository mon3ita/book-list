<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'disabled' => true
            ])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('passwordConfirmation', PasswordType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('currentPassword', PasswordType::class, [
                'attr' => ['style' => "margin-bottom: 10px"],
                'mapped' => false,
                'required' => $this->setRequiredPassword($options)
            ])
        ;

        if (in_array('ROLE_ADMIN', $options['roles'])) {
            $builder
                ->add('roles', ChoiceType::class, [
                'mapped' => false,
                'choices' => $this->getRoles(),
                'attr' => ['style' => "margin-bottom: 10px"]
            ]);
        }

        $builder->add('update', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'roles' => ['ROLE_USER']
        ]);
    }

    private function getRoles() {
        return array(
            'Admin' => 'ROLE_ADMIN',
            'User' => 'ROLE_USER'
        );
    }

    private function setRequiredPassword($options) {
        return !in_array('ROLE_ADMIN', $options['roles']); //fix
    }
}
