<?php

namespace App\Form;

use App\Entity\AuthorRating;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AuthorRatingFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('votes', ChoiceType::class, [
                'choices' => $this->getChoices(),
                'mapped' => false,
                'attr' => ['style' => 'margin: 10px 10px 10px 0;']
            ])
            ->add('vote', SubmitType::class, [
                'attr' => ['style' => 'margin: 10px 0 10px 0;']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AuthorRating::class,
        ]);
    }

    private function getChoices() {
        return array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5);
    }
}
