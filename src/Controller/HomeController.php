<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\User;
use App\Entity\Genre;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $books = $this->getDoctrine()
                        ->getRepository(Book::class)
                        ->count([]);

        $authors = $this->getDoctrine()
                        ->getRepository(Author::class)
                        ->count([]);

        $users = $this->getDoctrine()
                        ->getRepository(User::class)
                        ->count([]);

        $genres = $this->getDoctrine()
                        ->getRepository(Genre::class)
                        ->count([]);


        $lastBook = $this->getDoctrine()
                        ->getRepository(Book::class)
                        ->findOneBy([], ['id' => 'DESC']);

        $lastAuthor = $this->getDoctrine()
                        ->getRepository(Author::class)
                        ->findOneBy([], ['id' => 'DESC']);            

        return $this->render('home/index.html.twig', [
            "books" => $books,
            "authors" => $authors,
            "users" => $users,
            "genres" => $genres,
            "last_book" => $lastBook,
            "last_author" => $lastAuthor
        ]);
    }
}
