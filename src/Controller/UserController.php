<?php

namespace App\Controller;

use App\Entity\UserProfile;
use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Form\UserEditFormType;
use App\Form\UserProfileEditFormType;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('user/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/users/{username}", name="show_user")
     */
    public function profile($username) : Response {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $username
            ]);

        if(!$user) {
            throw  $this->createNotFoundException('User doesn\'t exist.');
        }

        $user_profile = $user->getUserProfile();

        return $this->render('user/profile.html.twig', [
            'user_profile' => $user_profile
        ]);
    }


    /**
     * @Route("/users/{username}/edit", name="edit_user")
     */
    public function edit($username, Request $request, UserPasswordEncoderInterface $passwordEncoder) : Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            "username" => $username
        ]);

        if(!$user)
            throw  $this->createNotFoundException('User doesn\'t exist.');

        $currentUser = $this->getUser();
        if($user && (!in_array('ROLE_ADMIN', $currentUser->getRoles()) && $user->getUsername() != $currentUser->getUsername()))
            return $this->redirect('/users/' . $this->getUser()->getUsername() . '/edit');

        $user_response = $this->updateUser($user, $request, $passwordEncoder);
        $user_profile_response = $this->updateUserProfile($user, $request);

        if(gettype($user_response) == "string")
            return $this->redirect($user_response);
        if(gettype($user_profile_response) == "string")
            return $this->redirect($user_profile_response);

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'user_form' => $user_response->createView(),
            'user_profile_form' => $user_profile_response->createView()
        ]);
    }

    private function updateUser($user, Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $currentUser = $this->getUser();
        $user_form = $this->createForm(UserEditFormType::class, $user, ['roles' => $currentUser->getRoles()]);

        $user_form->handleRequest($request);

        if ($user_form->isSubmitted() && $user_form->isValid()) {
            $entityManager = $this
                                ->getDoctrine()
                                ->getManager();

            if (!in_array('ROLE_ADMIN', $currentUser->getRoles()) &&
                (isset($user_form['plainPassword']) && isset($user_form['passwordConfirmation']))) {
                $path = $this->checkPassword($user, $user_form['currentPassword']->getData(), $passwordEncoder);
                if($path)
                    return $path;
                else {
                    $this->updatePassword($user, $user_form, $passwordEncoder);
                }
            }

            if(in_array('ROLE_ADMIN', $currentUser->getRoles())) {
                print($user_form['roles']->getData());
                $user->setRoles([$user_form['roles']->getData()]);
            }

            $entityManager->persist($user);
            $entityManager->flush();

            $path = '/users/' . $user->getUsername();
            return $path;
        }

        return $user_form;
    }

    private function checkPassword($user, $givenPassword, $passwordEncoder) {
        if(!$passwordEncoder->isPasswordValid($user, $givenPassword)) {
            $this->addFlash('password_error', 'User\'s password and given password don\'t match');
            
            $path = '/users/' . $this->getUser()->getUsername();
            return $path;
        }
    }

    private function updateUserProfile($user, Request $request) {
        $user_profile = $user->getUserProfile();

        $oldImage = $user_profile->getImage();

        $user_profile_form = $this->createForm(UserProfileEditFormType::class, $user_profile);
        $user_profile_form->handleRequest($request);
    
        $entityManager = $this->getDoctrine()->getManager();
        if ($user_profile_form->isSubmitted() && $user_profile_form->isValid()) {
            $this->setInformation($oldImage, $user_profile_form, $user_profile);

            $entityManager->persist($user_profile);
            $entityManager->flush();

            $path = '/users/' . $user->getUsername();
            return $path;
        }

        return $user_profile_form;
    }

    private function setImage($oldImage, $user_profile_form, $user_profile) {
        $image = $user_profile_form['image']->getData();

        if($image) {
            $originalImageName = pathinfo($image, PATHINFO_FILENAME);
            $safeImageName = preg_split("/\./", $image->getClientOriginalName())[0];

            $newImageName = $safeImageName . '-' . rand(0, 100) . '.' . $image->getClientOriginalExtension();

            $image_dir = 'images/' . $this->getUser()->getUsername();
            $image->move($image_dir, $newImageName);

            $user_profile->setImage($image_dir . '/' . $newImageName);
        } else {
            $user_profile->setImage($oldImage);
        }
    }

    private function setInformation($oldImage, $user_profile_form, $user_profile) {
        $this->setImage($oldImage, $user_profile_form, $user_profile);
    
        $user_profile->setFirstName($user_profile_form['first_name']->getData());

        $user_profile->setLastName($user_profile_form['last_name']->getData());
        $user_profile->setAboutMe($user_profile_form['about_me']->getData());
        $user_profile->setFavouriteGenres($user_profile_form['favourite_genres']->getData());

        $birthday = $user_profile_form['birthday']->getData();
        $user_profile->setBirthday($birthday);
    }

    private function updatePassword($user, $user_form, $passwordEncoder) {
        $entityManager = $this->getDoctrine()
                                ->getManager();

        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $user_form['plainPassword']->getData()
            )
        );

        $entityManager->persist($user);
        $entityManager->flush();
    }
}
