<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\User;
use App\Entity\Genre;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request): Response
    {   
        $term = $request->get('search');

        $authors = $this->getDoctrine()
                        ->getRepository(Author::class)
                        ->findBy(["name" => $term]);

        $books = $this->getDoctrine()
                      ->getRepository(Book::class)
                      ->findBy(["name" => $term]);

        $users = $this->getDoctrine()
                      ->getRepository(User::class)
                      ->findBy(["username" => $term]);

        $byGenre = $this->getDoctrine()
                        ->getRepository(Genre::class)
                        ->findOneBy(["name" => $term]);
        
        return $this->render('search/index.html.twig', [
            "authors" => $authors,
            "books" => $books,
            "users" => $users,
            "by_genre" => $byGenre
        ]);
    }
}
