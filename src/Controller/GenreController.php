<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Genre;
use App\Form\GenreFormType;

class GenreController extends AbstractController
{
    /**
     * @Route("/genres", name="genres")
     */
    public function index(): Response
    {
        $genres = $this->getDoctrine()->getRepository(Genre::class)->findAll();
        return $this->render('genre/index.html.twig', [
            'genres' => $genres
        ]);
    }

    /**
     * @Route("/genres/create", name="create_genre")
     */
    public function create(Request $request): Response
    {
        $genre = new Genre();
        $form = $this->createForm(GenreFormType::class, $genre);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($genre);
            $entityManager->flush();

            return $this->redirectToRoute('genres');
        }

        return $this->render('genre/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/genres/{genre}", name="show_genre")
     */
    public function show($genre, Request $request): Response
    {
        $genres = $this->getDoctrine()->getRepository(Genre::class)->findOneBy([
            'name' => $genre
        ]);

        if(!$genres) {
            throw  $this->createNotFoundException('Genre doesn\'t exist.');
        }

        return $this->render('genre/show.html.twig', [
            'books' => $genres->getBooks()
        ]);
    }
}
