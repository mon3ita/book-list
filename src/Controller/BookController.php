<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Book;
use App\Entity\BookRating;

use App\Form\BookFormType;
use App\Form\BookRatingFormType;

use App\Form\FavouriteFormType;

use App\Entity\Comment;
use App\Form\CommentFormType;

class BookController extends AbstractController
{
    /**
     * @Route("/books", name="books")
     */
    public function index(): Response
    {
        $authors = $this->getDoctrine()->getRepository(Author::class)->findAll();

        if(!count($authors)) {
            return $this->redirect('/authors/create');
        }

        $genres = $this->getDoctrine()->getRepository(Genre::class)->findAll();
        if(!count($genres)) {
            return $this->redirectToRoute('create_genre');
        }

        $books = $this->getDoctrine()->getRepository(Book::class)->findAll();

        return $this->render('book/index.html.twig', [
            'books' => $books
        ]);
    }

    /**
     * @Route("/books/create", name="create_book")
     */
    public function create(Request $request) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $book = new Book();

        $form = $this->createForm(BookFormType::class, $book);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $author = $this
                        ->getDoctrine()
                        ->getRepository(Author::class)
                        ->findOneBy(["name" => $form['authors']->getData()]);

            $genre = $this
                    ->getDoctrine()
                    ->getRepository(Genre::class)
                    ->findOneBy(["name" => $form['genres']->getData()]);

            $this->setImage($form, $book);
            $book->setAuthor($author);
            $book->setGenre($genre);
            $entityManager->persist($book);
            $entityManager->flush();

            $path = '/books/' . $book->getId();
            return $this->redirect($path);
        }

        return $this->render('book/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/books/{id}", name="show_book")
     */
    public function show($id, Request $request) {

        $book = $this->getDoctrine()->getRepository(Book::class)->find($id);
        if($book == null) {
            throw  $this->createNotFoundException('Book doesn\'t exist.');
        }

        extract($this->comments($book, $request));
        if($path) {
            return $this->redirect($path);
        }

        extract($this->favouriteBooks($book, $request));
        if($path)
            return $this->redirect($path);

        extract($this->bookRatings($book, $request));
        if($path)
            return $this->redirect($path);

        return $this->render('book/show.html.twig', [
            'book' => $book,
            'book_rating_form' => $bookRatingForm->createView(),
            'current_vote' => $currentBookRating,
            'book_ratings' => $bookRatings,
            'favourite_form' => $favouriteForm->createView(),
            'comment_form' => $commentForm->createView()
        ]);
    }

    /**
     * @Route("/books/{id}/edit", name="edit_book")
     */
    public function edit($id, Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $book = $this->getDoctrine()->getRepository(Book::class)->find($id);
        if($book == null) {
            return $this->redirect('/books');
        }

        $bookForm = $this->createForm(BookFormType::class, $book);
        $bookForm->handleRequest($request);

        if($bookForm->isSubmitted() && $bookForm->isValid()) {

            $this->setImage($bookForm, $book, $book->getCover());
            
            $entityManager = $this
                                ->getDoctrine()
                                ->getManager();

            $entityManager->persist($book);
            $entityManager->flush();

            $path = '/books/' . $book->getId();
            return $this->redirect($path);
        }

        return $this->render('book/edit.html.twig', [
            'book' => $book,
            'book_form' => $bookForm->createView()
        ]);
    }

    /**
     * @Route("/books/{id}/delete", name="delete_book")
     */
    public function delete($id) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
    }

    private function setImage($form, $book, $oldImage = '') {
        $image = $form['cover']->getData();

        if($image != null) {
            $originalImageName = pathinfo($image, PATHINFO_FILENAME);
            $safeImageName = preg_split("/\./", $image->getClientOriginalName())[0];

            $newImageName = $safeImageName . '-' . rand(0, 100) . '.' . $image->getClientOriginalExtension();

            $name = str_replace(' ', '_', $form['name']->getData());
            $name = preg_replace("/\#|\?/", '_', $name);
            $image_dir = 'images/books/' . $name;
            $image->move($image_dir, $newImageName);

            $book->setCover($image_dir . '/' . $newImageName);
        } else {
            $book->setCover($oldImage);
        }
    }

    private function updateRating($currentBookRating, $bookRatingForm) {
        $currentBookRating->setVote($bookRatingForm['votes']->getData());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentBookRating);
        $entityManager->flush();
    }

    private function setRating($book, $bookRating, $bookRatingForm) {
        $bookRating->setUsers($this->getUser());
        $bookRating->setBook($book);
        $bookRating->setVote($bookRatingForm['votes']->getData());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($bookRating);
        $entityManager->flush();
    }

    private function getVotes($book) {
        $ratings = $this->getDoctrine()
                        ->getRepository(BookRating::class)
                        ->findBy(['book' => $book->getId()]);

        $votes = array();
        foreach($ratings as $rating) {
            $votes[] = $rating->getVote();
        }

        return $votes;
    }

    private function comments($book, $request) {
        $comment = new Comment();
        $commentForm = $this->createForm(CommentFormType::class, $comment);
        $commentForm->handleRequest($request);

        $path = null;

        if($commentForm->isSubmitted() && $commentForm->isValid()) {
            $book->addComment($comment);

            $user = $this->getUser();
            $comment->setUsers($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->persist($book);
            $entityManager->flush();

            $path = '/books/' . $book->getId();
        }

        return array(
            "path" => $path,
            "commentForm" => $commentForm,
        );
    }

    private function favouriteBooks($book, $request) {

        $favouriteForm = null;
        $path = null;

        if($this->getUser() && $this->getUser()->getFavouriteBooks()->contains($book)) {
            $options = array(
                'class' => array('class' => 'btn btn-danger btn-sm'),
                'button_message' => 'remove_from_favourites'
            );


            $favouriteForm = $this
                            ->createForm(FavouriteFormType::class, 
                                $book, 
                                $options);
            $favouriteForm->handleRequest($request);
            $path = $this->removeFavourite($book, $favouriteForm);
        } else {
            $options['class'] = ['class' => 'btn btn-primary btn-sm'];

            $favouriteForm = $this
                            ->createForm(FavouriteFormType::class, 
                                $book, 
                                $options);
            $favouriteForm->handleRequest($request);
            $path = $this->addFavourite($book, $favouriteForm);
        }

        return array(
            "favouriteForm" => $favouriteForm,
            "path" => $path
        );
    }

    private function addFavourite($book, $favouriteBookForm) {

        if($favouriteBookForm->isSubmitted() && $favouriteBookForm->isValid()) {
            $user = $this->getUser();
            $user->addFavouriteBook($book);
            $book->addFavouriteTo($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($book);
            $entityManager->flush();

            $path = '/books/' . $book->getId();
            return $path;
        }

        return "";
    }

    private function removeFavourite($book, $favouriteBookForm) {
        if($favouriteBookForm->isSubmitted() && $favouriteBookForm->isValid()) {
            $user = $this->getUser();
            $user->removeFavouriteBook($book);
            $book->removeFavouriteTo($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($book);
            $entityManager->flush();

            $path = '/books/' . $book->getId();
            return $path;
        }

        return "";
    }

    private function bookRatings($book, $request) {
        $bookRating = new BookRating();
        $bookRatingForm = $this->createForm(BookRatingFormType::class, $bookRating);
        $bookRatingForm->handleRequest($request);

        $path = null;
        $currentBookRating = null;
        if($this->getUser()) {
            $currentBookRating = $this
                                    ->getDoctrine()
                                    ->getRepository(BookRating::class)
                                    ->findOneBy([
                                        'users' => $this->getUser()->getId()
                                    ]);
            if($currentBookRating != null && 
                $bookRatingForm->isSubmitted() && $bookRatingForm->isValid()) {

                $this->updateRating($currentBookRating, $bookRatingForm);
                $path = '/books/' . $book->getId();
            } else if($bookRatingForm->isSubmitted() && $bookRatingForm->isValid()) {
                $this->setRating($book, $bookRating, $bookRatingForm);
                $path = '/books/' . $book->getId();
            }
        }

        $bookRatings = $this->getVotes($book);

        return array(
            "bookRatingForm" => $bookRatingForm,
            "currentBookRating" => $currentBookRating,
            "bookRatings" => $bookRatings,
            "path" => $path
        );
    }
}
