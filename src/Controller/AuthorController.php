<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use App\Entity\Author;
use App\Form\AuthorFormType;

use App\Entity\AuthorRating;
use App\Form\AuthorRatingFormType;

use App\Entity\Comment;
use App\Form\CommentFormType;

use App\Form\FavouriteFormType;


class AuthorController extends AbstractController
{
    /**
     * @Route("/authors", name="authors")
     */
    public function index(): Response
    {

        $authors = $this->getDoctrine()
            ->getRepository(Author::class)
            ->findAll();

        return $this->render('author/index.html.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @Route("/authors/create", name="create_author")
     */
    public function create(ValidatorInterface $validator, Request $request) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $author = new Author();
        $form = $this->createForm(AuthorFormType::class, $author);
        $form->handleRequest($request);

        print($form->isValid());
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $this->setImage($form, $author);

            $entityManager->persist($author);
            $entityManager->flush();

            $path = '/authors/' . $author->getId();
            return $this->redirect($path);
        }

        return $this->render('author/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/authors/{id}", name="show_author")
    */
    public function show($id, Request $request) {

        $author = $this->getDoctrine()
                        ->getRepository(Author::class)
                        ->find($id);

        if($author == null) {
            throw  $this->createNotFoundException('Author doesn\'t exist.');
        }

        extract($this->comments($author, $request));
        if($path)
            return $this->redirect($path);

        extract($this->authorRating($author, $request));
        if($path)
            return $this->redirect($path);
        
        extract($this->favouriteAuthors($author, $request));
        if($path)
            return $this->redirect($path);


        return $this->render('author/show.html.twig', [
            'author' => $author,
            'author_rating_form' => $authorRatingForm->createView(),
            'current_vote' => $currentVote,
            'author_ratings' => $authorRatings,
            'comment_form' => $commentForm->createView(),
            'favourite_form' => $favouriteForm->createView(),
        ]);
    }

    /**
     * @Route("/authors/{id}/edit", name="edit_author")
     */
    public function edit($id, Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $author = $this
            ->getDoctrine()
            ->getRepository(Author::class)
            ->find($id);

        if($author == null) {
            return $this->redirect('/authors');
        }

        $authorForm = $this->createForm(AuthorFormType::class, $author);
        $authorForm->handleRequest($request);

        if($authorForm->isSubmitted() && $authorForm->isValid()) {
            $this->setImage($authorForm, $author, $author->getImage());

            $entityManager = $this
                                ->getDoctrine()
                                ->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            $path = '/authors/' . $author->getId();
            return $this->redirect($path);
        }

        return $this->render('author/edit.html.twig', [
            'author' => $author,
            'author_form' => $authorForm->createView()
        ]);
    }

    /**
     * @Route("/authors/{id}/delete", name="delete_author")
     */
    public function delete($id) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
    }

    private function setImage($form, $author, $oldImage = '') {
        $image = $form['image']->getData();

        if($image != null) {
            $originalImageName = pathinfo($image, PATHINFO_FILENAME);
            $safeImageName = preg_split("/\./", $image->getClientOriginalName())[0];

            $newImageName = $safeImageName . '-' . rand(0, 100) . '.' . $image->getClientOriginalExtension();

            $image_dir = 'images/authors/' . $form['name']->getData();
            $image->move($image_dir, $newImageName);

            $author->setImage($image_dir . '/' . $newImageName);
        } else {
            $author->setImage($oldImage);
        }
    }

    private function getVotes($author) {
        $ratings = $this->getDoctrine()
                        ->getRepository(AuthorRating::class)
                        ->findBy(['author' => $author->getId()]);

        $votes = array();
        foreach($ratings as $rating) {
            $votes[] = $rating->getVote();
        }

        return $votes;
    }

    private function updateRating($currentVote, $authorRatingForm) {
        $currentVote->setVote($authorRatingForm['votes']->getData());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentVote);
        $entityManager->flush();
    }

    private function setRating($author, $authorRating, $authorRatingForm) {
        $authorRating->setUsers($this->getUser());
        $authorRating->setAuthor($author);
        $authorRating->setVote($authorRatingForm['votes']->getData());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($authorRating);
        $entityManager->flush();
    }

    private function authorRating($author, $request) {
        $authorRating = new AuthorRating();
        $authorRatingForm = $this->createForm(AuthorRatingFormType::class, 
                                                $authorRating);
        $authorRatingForm->handleRequest($request);

        $path = null;
        $currentVote = null;

        if($this->getUser()) {
            $currentVote = $this
                            ->getDoctrine()
                            ->getManager()
                            ->getRepository(AuthorRating::class)
                            ->findOneBy([
                                'users' => $this->getUser()
                            ]);

            if($currentVote != null && $authorRatingForm->isSubmitted() 
                && $authorRatingForm->isValid()) {
                $this->updateRating($currentVote, $authorRatingForm);
                
                $path = '/authors/' . $author->getId();
            } else if($authorRatingForm->isSubmitted() && 
                        $authorRatingForm->isValid()) {
                $this->setRating($author, $authorRating, $authorRatingForm);

                $path = '/authors/' . $author->getId();
            }
        }

        $authorRatings = $this->getVotes($author);

        return array(
            "authorRatingForm" => $authorRatingForm,
            "authorRatings" => $authorRatings,
            "currentVote" => $currentVote,
            "path" => $path
        );
    }

    private function comments($author, $request) {
        $comment = new Comment();
        
        $commentForm = $this->createForm(CommentFormType::class, 
                                            $comment);
        $commentForm->handleRequest($request);

        $path = null;
        if($commentForm->isSubmitted() && $commentForm->isValid()) {
            $author->addComment($comment);

            $user = $this->getUser();
            $comment->setUsers($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->persist($author);
            $entityManager->flush();

            $path = '/authors/' . $author->getId();
        }

        return array(
            "commentForm" => $commentForm,
            "path" => $path
        );
    }

    private function favouriteAuthors($author, $request) {

        $favouriteForm = null;
        $path = null;

        if($this->getUser() && 
                $this->getUser()->getFavouriteAuthors()->contains($author)) {
            $options = array(
                'class' => array('class' => 'btn btn-danger btn-sm'),
                'button_message' => 'remove_from_favourites'
            );


            $favouriteForm = $this
                            ->createForm(FavouriteFormType::class, 
                                $author, 
                                $options);
            $favouriteForm->handleRequest($request);

            $path = $this->removeFavourite($author, $favouriteForm);
        } else {
            $options['class'] = ['class' => 'btn btn-primary btn-sm'];

            $favouriteForm = $this
                            ->createForm(FavouriteFormType::class, 
                                $author, 
                                $options);
            $favouriteForm->handleRequest($request);

            $path = $this->addFavourite($author, $favouriteForm);
        }

        return array(
            "favouriteForm" => $favouriteForm,
            "path" => $path
        );
    }

    private function addFavourite($author, $favouriteBookForm) {

        if($favouriteBookForm->isSubmitted() && $favouriteBookForm->isValid()) {
            $user = $this->getUser();
            
            $user->addFavouriteAuthor($author);
            $author->addFavouriteTo($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($author);
            $entityManager->flush();

            $path = '/authors/' . $author->getId();
            return $path;
        }

        return "";
    }

    private function removeFavourite($author, $favouriteBookForm) {
        if($favouriteBookForm->isSubmitted() && $favouriteBookForm->isValid()) {
            $user = $this->getUser();
            $user->removeFavouriteAuthor($author);
            $author->removeFavouriteTo($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($author);
            $entityManager->flush();

            $path = '/authors/' . $author->getId();
            return $path;
        }

        return "";
    }

}
