<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class RatingExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('calculate', [$this, 'calculate']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('calculate', [$this, 'calculate']),
        ];
    }

    public function calculate($ratings)
    {
        if(!$ratings)
            return 0;

        return array_sum($ratings) / count($ratings);
    }
}
