# Book List

```
1. Clone the repository
2. Set up a db
3. Run `composer install`
4. Run `php bin/console doctrine:database:create`
5. Run `php bin/console make:migration`
6. Run `php bin/console doctrine:migrations:migrate`
7. Run `php -S 127.0.0.1:8000 -t public`

Have fun :)
```